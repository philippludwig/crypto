mod openssl;

pub mod sha256;
pub mod hmac_sha512;
pub mod base64;

#[derive(Debug)]
pub enum Error {
	Base64(base64::Base64Error),
	Hmac(hmac_sha512::HmacError),
	Sha256(sha256::Sha256Error)
}
