use crate::openssl::*;
use super::Error;

#[derive(Debug)]
pub enum HmacError {
	KeyInitError,
	HmacSha512InitError,
	DigestInitFailed,
	DigestFailed,
	FinalizeGetLenFailed,
	FinalizeFailed
}

impl std::convert::From<HmacError> for Error {
	fn from(e: HmacError) -> Error {
		Error::Hmac(e)
	}
}

pub struct HmacKey {
	handle: *mut EVP_PKEY
}

impl HmacKey {
	pub fn new(key_bytes: &[u8]) -> Result<HmacKey, Error> {
		let handle = unsafe {
			EVP_PKEY_new_raw_private_key(EVP_PKEY_HMAC, std::ptr::null_mut(), key_bytes.as_ptr(), key_bytes.len())
		};

		match handle.is_null() {
			true => Err(HmacError::KeyInitError.into()),
			false => Ok(HmacKey { handle })
		}
	}

	fn handle(&self) -> *mut EVP_PKEY { self.handle }
}

impl Drop for HmacKey {
	fn drop(&mut self) {
		unsafe {
			EVP_PKEY_free(self.handle)
		};
	}
}

pub struct HmacSha512 {
	context: *mut EVP_MD_CTX,
	digesting: bool,
	key: HmacKey
}

impl HmacSha512 {
	pub fn new(key_bytes: &[u8]) -> Result<HmacSha512,Error> {
		let key = HmacKey::new(key_bytes)?;

		let context = unsafe { EVP_MD_CTX_new() };
		match context.is_null() {
			true => Err(HmacError::HmacSha512InitError.into()),
			false => Ok(HmacSha512 { context, digesting: false, key })
		}
	}

	pub fn digest(&mut self, data: &[u8]) -> Result <(),Error> {
		if !self.digesting {
			let result = unsafe {
				EVP_DigestSignInit(self.context, std::ptr::null_mut(),
					EVP_sha512(), std::ptr::null_mut(), self.key.handle())
			};

			if result != 1 {
				return Err(HmacError::DigestInitFailed.into());
			}
			self.digesting = true;
		}

		let result = unsafe {
			EVP_DigestUpdate(self.context, data.as_ptr(), data.len())
		};
		if result != 1 {
			return Err(HmacError::DigestFailed.into());
		}

		Ok(())
	}

	pub fn finalize(&mut self) -> Result<Vec<u8>,Error> {
		let mut sig_len: usize = 0;
		let result = unsafe {
			EVP_DigestSignFinal(self.context, std::ptr::null_mut(), &mut sig_len)
		};
		if result != 1 {
			return Err(HmacError::FinalizeGetLenFailed.into());
		}

		let mut buf = vec![0u8; sig_len];
		let result = unsafe {
			EVP_DigestSignFinal(self.context, buf.as_mut_ptr(), &mut sig_len)
		};
		if result != 1 {
			Err(HmacError::FinalizeFailed.into())
		} else {
			Ok(buf)
		}
	}
}

impl Drop for HmacSha512 {
	fn drop(&mut self) {
		unsafe {
			EVP_MD_CTX_free(self.context);
		}
	}
}

