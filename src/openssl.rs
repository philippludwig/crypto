use std::os::raw::{c_int};

#[repr(C)] pub struct EVP_MD_CTX { _private: [u8; 0] }
#[repr(C)] pub struct EVP_MD { _private: [u8; 0] }
#[repr(C)] pub struct EVP_PKEY { _private: [u8; 0] }
#[repr(C)] pub struct EVP_PKEY_CTX { _private: [u8; 0] }
#[repr(C)] pub struct EVP_ENCODE_CTX { _private: [u8; 0] }

pub const EVP_MAX_MD_SIZE: usize = 64;
pub const EVP_PKEY_HMAC:   c_int   = 855;

#[link(name = "crypto")]
#[allow(unused)]
extern {
	pub fn EVP_MD_CTX_new() -> *mut EVP_MD_CTX;
	pub fn EVP_DigestInit(ctx: *mut EVP_MD_CTX, evp_type: *const EVP_MD) -> c_int;
	pub fn EVP_sha256() -> *const EVP_MD;
	pub fn EVP_sha512() -> *const EVP_MD;
	pub fn EVP_DigestUpdate(ctx: *mut EVP_MD_CTX, d: *const u8, cnt: usize) -> c_int;
	pub fn EVP_DigestFinal_ex(ctx: *mut EVP_MD_CTX, md: *mut u8, s: *mut usize) -> c_int;
	pub fn EVP_DigestSignInit(ctx: *mut EVP_MD_CTX, pctx: *mut *mut EVP_PKEY_CTX,
		evp_type: *const EVP_MD, engine: *const std::ffi::c_void, pkey: *mut EVP_PKEY) -> c_int;
	pub fn EVP_DigestSignFinal(ctx: *mut EVP_MD_CTX, sig: *mut u8, sig_len: *mut usize) -> c_int;
	pub fn EVP_MD_CTX_free(ctx: *mut EVP_MD_CTX);
	pub fn EVP_PKEY_new_raw_private_key(key_type: c_int, e: *mut std::ffi::c_void,
		key: *const u8, keylen: usize) -> *mut EVP_PKEY;
	pub fn EVP_PKEY_free(key: *mut EVP_PKEY);

	pub fn EVP_ENCODE_CTX_new() -> *mut EVP_ENCODE_CTX;
	pub fn EVP_DecodeInit(ctx: *mut EVP_ENCODE_CTX);
	pub fn EVP_DecodeUpdate(ctx: *mut EVP_ENCODE_CTX, out: *mut u8, outl: *mut c_int,
		in_: *const u8, inl: c_int) -> c_int;
	pub fn EVP_DecodeFinal(ctx: *mut EVP_ENCODE_CTX, out: *mut u8, outl: *mut c_int);

	pub fn EVP_EncodeInit(ctx: *mut EVP_ENCODE_CTX);
	pub fn EVP_EncodeUpdate(ctx: *mut EVP_ENCODE_CTX, out: *mut u8, outl: *mut c_int,
		in_: *const u8, inl: c_int) -> c_int;
	pub fn EVP_EncodeFinal(ctx: *mut EVP_ENCODE_CTX, out: *mut u8, outl: *mut c_int);
	pub fn EVP_ENCODE_CTX_num(ctx: *mut EVP_ENCODE_CTX) -> std::os::raw::c_int;
}
