use crate::openssl::*;
use super::Error;

#[derive(Debug)]
pub enum Base64Error {
	DecodeFailed,
	EncodeFailed,
	InitFailed,
	MoreDataNeeded,
	UnknownError,
	Utf8Error
}

impl std::convert::From<Base64Error> for Error {
	fn from(e: Base64Error) -> Error {
		Error::Base64(e)
	}
}


pub struct Base64Decoder {
	context: *mut EVP_ENCODE_CTX
}

impl Drop for Base64Decoder {
	fn drop(&mut self) {
		unsafe {
			let mut null: i32 = 0;
			EVP_DecodeFinal(self.context, std::ptr::null_mut(), &mut null);
		}
	}
}

impl Base64Decoder {
	pub fn new() -> Result<Base64Decoder,Error> {
		let context = unsafe { EVP_ENCODE_CTX_new() };

		match context.is_null() {
			true => Err(Base64Error::InitFailed.into()),
			false => Ok(Base64Decoder { context })
		}
	}

	pub fn digest(&mut self, encoded: &str) -> Result<Vec<u8>,Error> {
		let encoded_data = encoded.as_bytes();
		let mut buf = vec![0u8; encoded_data.len()];
		let mut buf_size = buf.len() as i32;
		let ret = unsafe {
			EVP_DecodeUpdate(self.context, buf.as_mut_ptr(), &mut buf_size,
				encoded_data.as_ptr(), encoded_data.len() as i32)
		};

		match ret {
			0|1 => {  // According to the man page: A return value of 0 or 1 indicates successful processing.
				buf.truncate(buf_size as usize);
				Ok(buf)
			},
			-1 => Err(Base64Error::DecodeFailed.into()),
			_ => Err(Base64Error::UnknownError.into())
		}
	}
}


// ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
pub struct Base64Encoder {
	context: *mut EVP_ENCODE_CTX,
	encoded: Vec<u8>
}

impl Base64Encoder {
	pub fn new() -> Result<Base64Encoder,Error> {
		let context = unsafe { EVP_ENCODE_CTX_new() };

		match context.is_null() {
			true => Err(Base64Error::InitFailed.into()),
			false => {
				unsafe { EVP_EncodeInit(context) };
				Ok(Base64Encoder { context, encoded: Vec::new() })
			}
		}
	}

	pub fn digest(&mut self, input: &[u8]) -> Result<(),Error> {
		if input.len() == 0 {
			return Ok(());
		}

		// One block for every 48 bytes
		let output_num_blocks = f64::ceil(input.len() as f64 / 48.0);
		let output_size = output_num_blocks as usize * 66;
		//trace!("{}: {}", output_num_blocks, output_size);
		let mut output = vec![0u8; output_size];
		let mut out_len = output.len() as i32;
		//trace!("Output len: {}", out_len);

		let result = unsafe {
			EVP_EncodeUpdate(self.context, output.as_mut_ptr(), &mut out_len,
				input.as_ptr(), input.len() as i32)
		};

		//trace!("Out len 2: {}", out_len);

		if result != 1 {
			Err(Base64Error::EncodeFailed.into())
		} else {
			self.encoded.append(&mut output[0..(out_len as usize)].to_vec());
			Ok(())
		}
	}

	pub fn finalize(mut self) -> Result<String,Error> {
		let remaining = unsafe { EVP_ENCODE_CTX_num(self.context) };
		//trace!("Remaining: {}", remaining);
		if remaining > 0 {
			let mut output = [0u8; 66]; // No more than 66 bytes according to the man page.
			let mut out_len = output.len() as i32;
			unsafe {
				EVP_EncodeFinal(self.context, output.as_mut_ptr(), &mut out_len)
			};

			self.encoded.append(&mut output[0..(out_len as usize)].to_vec());
		}
		String::from_utf8(self.encoded).map_err(|_| Base64Error::Utf8Error.into())
	}
}

// ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

#[cfg(test)]
mod test {
	#[test]
	fn simple() {
		let mut decoder = super::Base64Decoder::new().unwrap();
		let result = decoder.digest("dWllaWF1ZWl1ZWFhYWFhYWFhYWFhYWFhYWFhYWFhYQ==").unwrap();
		assert_eq!("uieiaueiueaaaaaaaaaaaaaaaaaaaaa", String::from_utf8(result).unwrap());
	}
}

#[cfg(test)]
mod test_encoder {
	#[test]
	fn rfc() {
		let test = [ ["f", "Zg==\n"], ["fo", "Zm8=\n"], [ "", "" ],
			["foo","Zm9v\n"],
			["foob","Zm9vYg==\n"],
			["fooba","Zm9vYmE=\n"],
			["foobar","Zm9vYmFy\n"],
		];
		for t in test.iter() {
			let mut encoder = super::Base64Encoder::new().unwrap();
			encoder.digest(t[0].as_bytes()).unwrap();
			let result = encoder.finalize().unwrap();
			assert_eq!(result, t[1]);
		}
	}
}
