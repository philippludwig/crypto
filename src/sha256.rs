use crate::openssl::*;
use crate::*;

#[derive(Debug)]
pub enum Sha256Error {
	ContextInitFailed,
	Sha256NotAvailable,
	DigestInitFailed,
	DigestFailed,
	FinalizeFailed
}

impl std::convert::From<sha256::Sha256Error> for Error {
	fn from(e: sha256::Sha256Error) -> Error {
		Error::Sha256(e)
	}
}


pub struct Sha256 {
	context: * mut EVP_MD_CTX,
	sha256_handle: * const EVP_MD,
	digesting: bool
}

impl Sha256 {
	pub fn new() -> Result<Sha256, Error> {
		let context = unsafe { EVP_MD_CTX_new() };
		if context.is_null() {
			return Err(Sha256Error::ContextInitFailed.into());
		}

		let sha256_handle = unsafe { EVP_sha256() };
		if sha256_handle.is_null() {
			unsafe { EVP_MD_CTX_free(context) };
			return Err(Sha256Error::Sha256NotAvailable.into());
		}

		Ok(Sha256 {
			context,
			sha256_handle,
			digesting: false
		})
	}

	pub fn digest(&mut self, data: &[u8]) -> Result<(), Error> {
		if !self.digesting {
			let result = unsafe { EVP_DigestInit(self.context, self.sha256_handle) };
			if result != 1 {
				return Err(Sha256Error::DigestInitFailed.into());
			}
			self.digesting = true;
		}

		let result = unsafe { EVP_DigestUpdate(self.context, data.as_ptr(), data.len()) };
		if result != 1 {
			return Err(Sha256Error::DigestFailed.into());
		}

		Ok(())
	}

	pub fn finalize(&mut self) -> Result<Vec<u8>, Error> {
		let mut bytes_written: usize = 0;
		let mut buf = [0u8; EVP_MAX_MD_SIZE];
		let result = unsafe { EVP_DigestFinal_ex(self.context, buf.as_mut_ptr(), &mut bytes_written) };
		if result != 1 {
			return Err(Sha256Error::FinalizeFailed.into());
		}
		self.digesting = false;

		Ok(buf[0..bytes_written].to_vec())
	}
}

impl Drop for Sha256 {
	fn drop(&mut self) {
		unsafe { EVP_MD_CTX_free(self.context) };
	}
}
